package edu.hneu.mjt.ivanytskyi.ticket_1_22.Service;

import edu.hneu.mjt.ivanytskyi.ticket_1_22.Entity.TouristGuide;
import edu.hneu.mjt.ivanytskyi.ticket_1_22.Model.TouristGuideDto;
import edu.hneu.mjt.ivanytskyi.ticket_1_22.Repository.TouristGuideRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TouristGuideService {

    @Autowired
    private TouristGuideRepository touristGuideRepository;

    public List<TouristGuideDto> getAllTouristGuides() {
        return touristGuideRepository.findAll().stream()
                .map(TouristGuideDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<TouristGuideDto> getTouristGuideById(Long id) {
        return touristGuideRepository.findById(id).map(TouristGuideDto::fromEntity);
    }

    public TouristGuideDto createTouristGuide(TouristGuideDto touristGuideDto) {
        TouristGuide touristGuide = touristGuideDto.toEntity();
        touristGuide = touristGuideRepository.save(touristGuide);
        return TouristGuideDto.fromEntity(touristGuide);
    }

    public TouristGuideDto updateTouristGuide(TouristGuideDto touristGuideDto) {
        TouristGuide touristGuide = touristGuideDto.toEntity();
        touristGuide = touristGuideRepository.save(touristGuide);
        return TouristGuideDto.fromEntity(touristGuide);
    }

    public void deleteTouristGuide(Long id) {
        touristGuideRepository.deleteById(id);
    }
}