package edu.hneu.mjt.ivanytskyi.ticket_1_22.Controller;

import edu.hneu.mjt.ivanytskyi.ticket_1_22.Model.TourDto;
import edu.hneu.mjt.ivanytskyi.ticket_1_22.Service.TourService;
import edu.hneu.mjt.ivanytskyi.ticket_1_22.Service.TouristGuideService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/tours")
public class TourController {

    @Autowired
    private TourService tourService;

    @Autowired
    private TouristGuideService touristGuideService;

    @GetMapping
    public ResponseEntity<List<TourDto>> getAllTours() {
        List<TourDto> tours = tourService.getAllTours();
        return ResponseEntity.ok(tours);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TourDto> getTourById(@PathVariable Long id) {
        Optional<TourDto> tour = tourService.getTourById(id);
        return tour.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<?> createTour(@Valid @RequestBody TourDto tourDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }

        // Додаткова валідація: Перевірка, чи тривалість додатнє число
        if (tourDto.getDuration() <= 0) {
            return ResponseEntity.badRequest().body("Тривалість туру повинна бути додатнім числом");
        }

        tourService.createTour(tourDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateTour(@PathVariable Long id, @Valid @RequestBody TourDto tourDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }

        // Додаткова валідація: Перевірка, чи тривалість додатнє число
        if (tourDto.getDuration() <= 0) {
            return ResponseEntity.badRequest().body("Тривалість туру повинна бути додатнім числом");
        }

        tourDto.setId(id);
        tourService.updateTour(tourDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTour(@PathVariable Long id) {
        tourService.deleteTour(id);
        return ResponseEntity.noContent().build();
    }
}
