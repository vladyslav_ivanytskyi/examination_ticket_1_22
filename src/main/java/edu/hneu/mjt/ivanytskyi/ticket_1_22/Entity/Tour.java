package edu.hneu.mjt.ivanytskyi.ticket_1_22.Entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Data;

@Entity
@Data
public class Tour {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotBlank(message = "Країна туру не може бути порожньою")
    private String country;

    @Column(nullable = false)
    @NotNull(message = "Тривалість туру не може бути порожньою")
    @Positive(message = "Тривалість туру повинна бути додатнім числом")
    private Integer duration;

    @Column(nullable = false)
    @NotNull(message = "Вартість туру не може бути порожньою")
    @Positive(message = "Вартість туру повинна бути додатнім числом")
    private Double cost;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tourist_guide_id", nullable = false)
    private TouristGuide touristGuide;
}
