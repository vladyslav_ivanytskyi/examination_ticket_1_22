package edu.hneu.mjt.ivanytskyi.ticket_1_22.Repository;

import edu.hneu.mjt.ivanytskyi.ticket_1_22.Entity.TouristGuide;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TouristGuideRepository extends JpaRepository<TouristGuide, Long> {
}
