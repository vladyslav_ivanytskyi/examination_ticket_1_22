package edu.hneu.mjt.ivanytskyi.ticket_1_22.Entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class TouristGuide {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotBlank(message = "ПІБ гіда не може бути порожнім")
    private String fullName;

    @Column(nullable = false)
    @NotBlank(message = "Мови гіда не можуть бути порожніми")
    private String languages;

    @Column(nullable = false)
    @NotBlank(message = "Країни гіда не можуть бути порожніми")
    private String countries;

    @Column(nullable = false)
    @NotBlank(message = "Номер телефону гіда не може бути порожнім")
    @Pattern(regexp = "^\\+380\\(\\d{2}\\)\\d{3}-\\d{2}-\\d{2}$",message = "Номер телефону має бути у форматі +38(0xx) xxx-xx-xx")
    private String phoneNumber;

    @OneToMany(mappedBy = "touristGuide", cascade = CascadeType.ALL)
    private List<Tour> tours = new ArrayList<>();
}