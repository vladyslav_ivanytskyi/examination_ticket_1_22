package edu.hneu.mjt.ivanytskyi.ticket_1_22.Model;

import edu.hneu.mjt.ivanytskyi.ticket_1_22.Entity.Tour;
import edu.hneu.mjt.ivanytskyi.ticket_1_22.Entity.TouristGuide;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class TourDto {
    private Long id;
    @NotBlank(message = "Країна туру не може бути порожньою")
    private String country;
    @NotNull(message = "Тривалість туру не може бути порожньою")
    @Positive(message = "Тривалість туру повинна бути додатнім числом")
    private Integer duration;
    @NotNull(message = "Вартість туру не може бути порожньою")
    @Positive(message = "Вартість туру повинна бути додатнім числом")
    private Double cost;
    private TouristGuideDto touristGuide;

    public Tour toEntity() {
        Tour tour = new Tour();
        tour.setId(id);
        tour.setCountry(country);
        tour.setDuration(duration);
        tour.setCost(cost);
        if (touristGuide != null) {
            TouristGuide touristGuideEntity = new TouristGuide();
            touristGuideEntity.setId(touristGuide.getId());
            tour.setTouristGuide(touristGuideEntity);
        }
        return tour;
    }

    public static TourDto fromEntity(Tour tour) {
        TourDto dto = new TourDto();
        dto.setId(tour.getId());
        dto.setCountry(tour.getCountry());
        dto.setDuration(tour.getDuration());
        dto.setCost(tour.getCost());
        if (tour.getTouristGuide() != null) {
            TouristGuideDto touristGuideDto = new TouristGuideDto();
            touristGuideDto.setId(tour.getTouristGuide().getId());
            touristGuideDto.setFullName(tour.getTouristGuide().getFullName());
            dto.setTouristGuide(touristGuideDto);
        }
        return dto;
    }
}
