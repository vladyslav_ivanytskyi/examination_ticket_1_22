package edu.hneu.mjt.ivanytskyi.ticket_1_22.Controller;

import edu.hneu.mjt.ivanytskyi.ticket_1_22.Model.TouristGuideDto;
import edu.hneu.mjt.ivanytskyi.ticket_1_22.Service.TouristGuideService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/tourist-guides")
public class TouristGuideController {

    @Autowired
    private TouristGuideService touristGuideService;

    @GetMapping
    public ResponseEntity<List<TouristGuideDto>> getAllTouristGuides() {
        List<TouristGuideDto> touristGuides = touristGuideService.getAllTouristGuides();
        return ResponseEntity.ok(touristGuides);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TouristGuideDto> getTouristGuideById(@PathVariable Long id) {
        Optional<TouristGuideDto> touristGuide = touristGuideService.getTouristGuideById(id);
        return touristGuide.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<?> createTouristGuide(@Valid @RequestBody TouristGuideDto touristGuideDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }

        touristGuideService.createTouristGuide(touristGuideDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateTouristGuide(@PathVariable Long id, @Valid @RequestBody TouristGuideDto touristGuideDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(bindingResult.getAllErrors());
        }

        touristGuideDto.setId(id);
        touristGuideService.updateTouristGuide(touristGuideDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTouristGuide(@PathVariable Long id) {
        touristGuideService.deleteTouristGuide(id);
        return ResponseEntity.noContent().build();
    }
}


