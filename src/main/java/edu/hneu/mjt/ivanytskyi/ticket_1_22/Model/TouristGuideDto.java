package edu.hneu.mjt.ivanytskyi.ticket_1_22.Model;

import edu.hneu.mjt.ivanytskyi.ticket_1_22.Entity.TouristGuide;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class TouristGuideDto {
    private Long id;
    @NotBlank(message = "ПІБ гіда не може бути порожнім")
    private String fullName;
    @NotBlank(message = "Мови гіда не можуть бути порожніми")
    private String languages;
    @NotBlank(message = "Країни гіда не можуть бути порожніми")
    private String countries;
    @NotBlank(message = "Номер телефону гіда не може бути порожнім")
    @Pattern(regexp = "^\\+380\\(\\d{2}\\)\\d{3}-\\d{2}-\\d{2}$",message = "Номер телефону має бути у форматі +38(0xx) xxx-xx-xx")
    private String phoneNumber;

    public static TouristGuideDto fromEntity(TouristGuide entity) {
        TouristGuideDto dto = new TouristGuideDto();
        dto.setId(entity.getId());
        dto.setFullName(entity.getFullName());
        dto.setLanguages(entity.getLanguages());
        dto.setCountries(entity.getCountries());
        dto.setPhoneNumber(entity.getPhoneNumber());
        return dto;
    }

    public TouristGuide toEntity() {
        TouristGuide entity = new TouristGuide();
        entity.setId(this.id);
        entity.setFullName(this.fullName);
        entity.setLanguages(this.languages);
        entity.setCountries(this.countries);
        entity.setPhoneNumber(this.phoneNumber);
        return entity;
    }
}
