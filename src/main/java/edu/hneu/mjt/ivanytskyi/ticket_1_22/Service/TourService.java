package edu.hneu.mjt.ivanytskyi.ticket_1_22.Service;

import edu.hneu.mjt.ivanytskyi.ticket_1_22.Entity.Tour;
import edu.hneu.mjt.ivanytskyi.ticket_1_22.Model.TourDto;
import edu.hneu.mjt.ivanytskyi.ticket_1_22.Repository.TourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TourService {

    @Autowired
    private TourRepository tourRepository;

    public List<TourDto> getAllTours() {
        return tourRepository.findAll().stream()
                .map(TourDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<TourDto> getTourById(Long id) {
        return tourRepository.findById(id).map(TourDto::fromEntity);
    }

    public TourDto createTour(TourDto tourDto) {
        Tour tour = tourDto.toEntity();
        tour = tourRepository.save(tour);
        return TourDto.fromEntity(tour);
    }

    public TourDto updateTour(TourDto tourDto) {
        Tour tour = tourDto.toEntity();
        tour = tourRepository.save(tour);
        return TourDto.fromEntity(tour);
    }

    public void deleteTour(Long id) {
        tourRepository.deleteById(id);
    }
}
