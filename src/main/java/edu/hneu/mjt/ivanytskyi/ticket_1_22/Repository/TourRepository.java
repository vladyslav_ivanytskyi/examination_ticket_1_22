package edu.hneu.mjt.ivanytskyi.ticket_1_22.Repository;

import edu.hneu.mjt.ivanytskyi.ticket_1_22.Entity.Tour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TourRepository extends JpaRepository<Tour, Long> {
}
