package edu.hneu.mjt.ivanytskyi.ticket_1_22;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExaminationTicket122Application {

    public static void main(String[] args) {
        SpringApplication.run(ExaminationTicket122Application.class, args);
    }

}
